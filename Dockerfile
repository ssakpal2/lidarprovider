FROM norarosmarin/framework:latest

RUN apt-get update

RUN apt-get -y install ros-iron-pcl-conversions clang && apt-get clean

RUN apt-get -y install llvm lcov && apt-get clean 	

# Install code coverage tools
RUN apt-get -y install gcovr && apt-get clean

# Set Clang as the default compiler
ENV CC=clang
ENV CXX=clang++

RUN bash -c "cd .. && rosdep install --from-paths src --ignore-src -r -y"

RUN bash -c "cd /root && apt install -y wget libprotobuf-dev libprotobuf23 && apt install -y git build-essential libprotobuf-dev libprotoc-dev protobuf-compiler && apt install -y git build-essential autoconf automake libtool curl unzip"

RUN bash -c "cd /root && git clone --recursive https://github.com/Blickfeld/blickfeld-scanner-lib.git && mkdir blickfeld-scanner-lib/build && cd blickfeld-scanner-lib/build && cmake .. && make && make install"

RUN mkdir lidar_provider

COPY /code /root/ros2_workspace/src/lidar_provider

RUN bash -c "cd /root/ros2_workspace/src/lidar_provider && source /opt/ros/iron/setup.bash && colcon build --symlink-install"


ENTRYPOINT [ "/bin/bash", "-c", "cd /root/ros2_workspace/src/lidar_provider && export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/root/blickfeld-scanner-lib/build && source /opt/ros/iron/setup.bash && source /root/ros2_workspace/src/lidar_provider/install/setup.bash && lcov --capture --directory . --output-file coverage.info && lcov --remove coverage.info '/usr/*' --output-file coverage.info && lcov --list coverage.info && genhtml coverage.info --output-directory coverage_report && ros2 run lidar_provider fuzzer_node" ]



